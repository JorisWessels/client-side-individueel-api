const User = require('../models/user');
require('dotenv').config();

const jwt = require('jsonwebtoken');

const neo = require('../../neo');


module.exports = {
    async getAll(req, res, next) {
        console.log("GetALl aangeroepen");

        const session = neo.session()
        await session.run(neo.getAllUsers)
            .then(users => {
                const splitUsers = [];
                users.records[0]._fields[0].forEach(user => {
                    const obj = {
                        id: user.identity.low,
                        name: user.properties.name,
                        email: user.properties.email,
                    }
                    splitUsers.push(obj);
                })
                return splitUsers;
            })
            .then(users => res.send(users))
            .catch(next)
        await session.close();
    },

    getUserIdFromToken(req, res, next) {
        const decoded = jwt.verify(req.params.token, process.env.JWT_SECRET);
        res.send({
            id: decoded.id
        }).catch(next);
    },

    async getUserById(req, res, next) {
        const session = neo.session()
        const id = parseInt(req.params.id);
        await session.run(neo.getUserById, {id})
            .then(user => {
                res.send({
                    id: id,
                    name: user.records[0]._fields[0].properties.name,
                    email: user.records[0]._fields[0].properties.email,
                });
            }).catch(next);
    },

    async login(req, res, next) {
        const session = neo.session()
        const email = req.body.email;
        const password = req.body.password;
        // console.log("User mail: " + email);
        await session.run(neo.getUser, {email})
            .then(user => {
                if (user.records[0] === undefined) {
                    return res.status(400).send({
                        status: 400,
                        error: "email not found",
                    });
                } else if (user.records[0]._fields[0].properties.password !== password) {
                    return res.status(400).send({
                        status: 400,
                        error: "password wrong",
                    });
                } else {
                    // console.log(user.records[0]._fields[0].properties);
                    // console.log(user.records[0]._fields[0].identity.low);
                    const token = jwt.sign({
                        id: user.records[0]._fields[0].identity.low
                    }, process.env.JWT_SECRET);
                    res.send({
                        name: user.records[0]._fields[0].properties.name,
                        email: user.records[0]._fields[0].properties.email,
                        token: token
                    })
                }
            })
            .catch(next)
        await session.close();
    },

    async create(req, res, next) {
        const session = neo.session()
        const userProps = req.body;
        await session.run(neo.createUser, userProps)
            .then(user => res.send(user))
            .catch(next)
        await session.close();
    },

    async follow(req, res, next) {
        console.log("Follow aangeroepen")
        const session = neo.session()
        const name = req.body.name;
        const followName = req.body.followName;
        await session.run(neo.followUser, {name, followName})
            .then(user => res.send(user))
            .catch(next)
        await session.close();
    },

    async unfollow(req, res, next) {
        const session = neo.session()
        const name = req.body.name;
        const unfollowName = req.body.unfollowName;
        console.log("Name:", name)
        console.log("UnfollowName:", unfollowName)
        await session.run(neo.unfollowUser, {name, unfollowName})
            .then(user => res.send(user))
            .catch(next)
        await session.close();
    },

    async followList(req, res, next) {
        console.log("FollowList aangeroepen");
        const session = neo.session()
        const id = parseInt(req.params.id);
        await session.run(neo.followList, {id})
            .then(users => {
                const splitUsers = [];
                users.records[0]._fields[0].forEach(user => {
                    const obj = {
                        id: user.identity.low,
                        name: user.properties.name,
                        email: user.properties.email,
                    }
                    splitUsers.push(obj);
                })
                return splitUsers;
            })
            .then(users => res.send(users))
            .catch(next);
        await session.close();
    },
};


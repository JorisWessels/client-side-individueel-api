const Comment = require('../models/comment');
const Item = require('../models/item');

module.exports = {
    getAll(req, res, next) {
        Comment.find()
            .then(comments => res.send(comments))
            .catch(next);
    },

    getAllCommentsByUserId(req, res, next) {
        const userId = req.params.id;
        // console.log("getAllCommentsByUserId aangeroepen");
        // console.log("UserId:", userId);
        Item.aggregate([
            {
                $project: {
                    "comments": {
                        $filter: {
                            input: "$comments",
                            as: "comment",
                            cond: {
                                $eq: [ "$$comment.userid", parseInt(userId)]
                            }
                        }
                    }
                }
            }
        ]).then(comments => res.send(comments))
            .catch(next);
    },

    getOne(req, res, next) {
        const commentId = req.params.id;
        Item.findOne({"comments._id": commentId})
            .then(item => item.comments
                .forEach(comment => {
                    if (comment._id.toString() === commentId.toString()){
                        res.send(comment);
                    }
                }))
            .catch(next);
    },

    create(req, res, next) {
        const commentProps = req.body;
        Comment.create(commentProps)
            .then(comment => res.send(comment))
            .catch(next)
    },

    edit(req, res, next) {
        const commentId = req.params.id;
        const commentProps = req.body;

        Item.findOne({"comments._id": commentId})
            .then(item => {
                const comment = item.comments.id(commentId);
                comment.set(commentProps);
                return item.save();
            })
            .then(item => res.send(item))
            .catch(next);
    },

    delete(req, res, next) {
        const commentId = req.params.id;

        Item.findOne({"comments._id": commentId})
            .then(item => {
                const comment = item.comments.id(commentId);
                comment.remove();
                return item.save();
            })
            .then(item => res.status(204).send(item))
            .catch(next);

    }
};

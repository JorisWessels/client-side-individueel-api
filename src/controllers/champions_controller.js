const Champion = require('../models/champion');

module.exports = {
    greeting(req, res) {
        res.send({
            urls: {
                getAllChampions: 'https://lolstats-api.herokuapp.com/api/champions',
                getOneChampion: 'https://lolstats-api.herokuapp.com/api/champions/:id',
                getAllItems: 'https://lolstats-api.herokuapp.com/api/items/:id',
                getOneItem: 'https://lolstats-api.herokuapp.com/api/items/:id'
            }
        });
    },

    getAll(req, res, next) {
        console.log('getAll Champions aangeroepen');
        Champion.find()
            .then(champions => res.send(champions))
            .catch(next);
    },

    getOne(req, res, next) {
        const championId = req.params.id;
        Champion.findById({_id: championId})
            .then(champion => res.send(champion))
            .catch(next);
    },

    create(req, res, next) {
        console.log('Create Champions aangeroepen');
        const championProps = req.body;
        Champion.create(championProps)
            .then(champion => res.send(champion))
            .catch(next)
    },

    edit(req, res, next) {
        console.log('Edit Champions aangeroepen');
        const championId = req.params.id;
        const championProps = req.body;

        Champion.findByIdAndUpdate({_id: championId}, championProps)
            .then(() => Champion.findById({_id: championId}))
            .then(champion => res.send(champion))
            .catch(next);
    },

    delete(req, res, next) {
        const championId = req.params.id;
        Champion.findByIdAndRemove({_id: championId})
            .then(champion => res.status(204).send(champion))
            .catch(next);
    }
};

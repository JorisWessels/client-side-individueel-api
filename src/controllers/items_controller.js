const Item = require('../models/item');
const Comment = require('../models/comment');
const User = require('../models/user');
const neo = require('../../neo');


module.exports = {
    getAll(req, res, next) {
        console.log('getAllItems aangeroepen')
        Item.find()
            .then(items => res.send(items))
            .catch(next);
    },

    getOne(req, res, next) {
        const itemId = req.params.id;
        Item.findById({_id: itemId})
            .then(item => res.send(item))
            .catch(next);
    },

    create(req, res, next) {
        const itemProps = req.body;
        Item.create(itemProps)
            .then(item => res.send(item))
            .catch(next)
    },

    addComment(req, res, next) {
        console.log('addComment aangeroepen');

        const itemId = req.params.id;
        const commentBody = req.body;
        Item.findById({_id: itemId})
            .then(item => {
                item.comments.push({
                    content: commentBody.content,
                    rating: commentBody.rating,
                    dateofcomment: commentBody.dateofcomment,
                    userid: commentBody.userid.id,
                })
                return item.save()
            })
            .then(item => res.status(200).json({response: item}))
            .catch(next);
    },

    edit(req, res, next) {
        const itemId = req.params.id;
        const itemProps = req.body;

        // console.log(itemProps);
        Item.findByIdAndUpdate({_id: itemId}, itemProps)
            .then(() => Item.findById({_id: itemId}))
            .then(item => res.send(item))
            .catch(next);
    },

    delete(req, res, next) {
        const itemId = req.params.id;
        Item.findByIdAndRemove({_id: itemId})
            .then(item => res.status(204).send(item))
            .catch(next);
    }
};

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CommentSchema = require('./comment');

const ChampionSchema = new Schema(
    {
        key: {
            type: String,
            required: [true, "Key is required."]
        },
        name: {
            type: String,
            required: [true, "Name is required."]
        },
        title: {
            type: String,
            required: [true, "Title is required."]
        },
        tags: {
            type: [String],
            required: [true, "Icon is required."]
        },
        stats: {
            hp: {
                type: Number,
                required: [true, "Hp is required."]
            },
            hpperlevel: {
                type: Number,
                required: [true, "Hp per level is required"]
            },
            mp: {
                type: Number,
                required: [true, "Mp is required."]
            },
            mpperlevel: {
                type: Number,
                required: [true, "Mp per level is required."]
            },
            movespeed: {
                type: Number,
                required: [true, "Movespeed is required."]
            },
            armor: {
                type: Number,
                required: [true, "Armor is required."]
            },
            armorperlevel: {
                type: Number,
                required: [true, "Armor per level is required."]
            },
            spellblock: {
                type: Number,
                required: [true, "Spellblock is required."]
            },
            spellblockperlevel: {
                type: Number,
                required: [true, "Spellblock per level is required."]
            },
            attackrange: {
                type: Number,
                required: [true, "Attack range is required."]
            },
            hpregen: {
                type: Number,
                required: [true, "Hp regen is required."]
            },
            hpregenperlevel: {
                type: Number,
                required: [true, "Hp regen per level is required."]
            },
            mpregen: {
                type: Number,
                required: [true, "Mp regen is required."]
            },
            mpregenperlevel: {
                type: Number,
                required: [true, "Mp regen per level is required."]
            },
            crit: {
                type: Number,
                required: [true, "Crit is required."]
            },
            critperlevel: {
                type: Number,
                required: [true, "Crit per level is required."]
            },
            attackdamage: {
                type: Number,
                required: [true, "Attack damage is required."]
            },
            attackdamageperlevel: {
                type: Number,
                required: [true, "Attack damage per level is required."]
            },
            attackspeedperlevel: {
                type: Number,
                required: [true, "Attack speed per level is required."]
            },
            attackspeed: {
                type: Number,
                required: [true, "Attack speed is required."]
            },
        },
        icon: {
            type: String,
            required: [true, "Icon is required."]
        },
        sprite: {
            url: {
                type: String,
                default: ""
            },
            x: {
                type: Number,
                default: 0
            },
            y: {
                type: Number,
                default: 0
            }
        },
        description: {
            type: String,
            required: [true, "Description is required."]
        },
        comments: [CommentSchema]
    });

const Champion = mongoose.model('champion', ChampionSchema);

module.exports = Champion;

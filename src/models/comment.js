const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema(
    {
        dateofcomment: Date,
        content: {
            type: String,
            required: [true, "Content is required"]
        },
        rating: {
            type: Number,
            default: 0,
        },
        user: Number,
        userid: Number
    });

module.exports = CommentSchema;

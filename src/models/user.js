const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CommentSchema = require('./comment');

const UserSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, "Name is required"]
        },
        password: {
            type: String,
            required: [true, "Password is required"]
        },
        email: {
            type: String,
            required: [true, "Email is required"],
            unique: [true, 'A user needs to have a unique email']
        },
    });

UserSchema.pre('remove', function(next) {
    const Comment = mongoose.model('comment')

    Comment.updateMany({}, {$pull: {'comments': {'userid': this._id}}})
        .then(() => next())
})

const User = mongoose.model('user', UserSchema);

module.exports = User;

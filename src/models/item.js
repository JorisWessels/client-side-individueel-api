const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const CommentSchema = require('./comment');

const ItemSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, "Name is required"]
        },
        description: {
            type: String,
            required: [true, "Description is required"]
        },
        cost: {
            type: Number,
            required: [true, "Cost is required"]
        },
        icon: {
            type: String,
            default: "",
        },
        stats: {
            hp: {
                type: Number,
                default: 0,
            },
            mp: {
                type: Number,
                default: 0,
            },
            movespeed: {
                type: Number,
                default: 0,
            },
            armor: {
                type: Number,
                default: 0,
            },
            spellblock: {
                type: Number,
                default: 0,
            },
            hpregen: {
                type: Number,
                default: 0,
            },
            mpregen: {
                type: Number,
                default: 0,
            },
            crit: {
                type: Number,
                default: 0,
            },
            attackdamage: {
                type: Number,
                default: 0,
            },
            attackspeed: {
                type: Number,
                default: 0,
            },
        },
        comments: {
            type: [CommentSchema],
            default: [],
            required: true
        }
    });

const Item = mongoose.model('item', ItemSchema);

module.exports = Item;

const ChampionController = require('../controllers/champions_controller');
const ItemController = require('../controllers/items_controller');
const CommentController = require('../controllers/comments_controller');
const UserController = require('../controllers/users_controller');

module.exports = (app) => {
    // to the route ../api
    app.get('/api', ChampionController.greeting);

    // to the route ../api/champions
    app.get('/api/champions', ChampionController.getAll);
    app.post('/api/champions', ChampionController.create);
    app.put('/api/champions/:id', ChampionController.edit);
    app.get('/api/champions/:id', ChampionController.getOne);
    app.delete('/api/champions/:id', ChampionController.delete);

    // to the route ../api/items
    app.get('/api/items', ItemController.getAll);
    app.post('/api/items', ItemController.create);
    app.put('/api/items/:id', ItemController.edit);
    app.get('/api/items/:id', ItemController.getOne);
    app.delete('/api/items/:id', ItemController.delete);
    app.put('/api/items/:id/addcomment', ItemController.addComment);

    // to the route ../api/comments
    // app.get('/api/comments', CommentController.getAll);
    // app.post('/api/comments', CommentController.create);
    app.put('/api/comments/:id', CommentController.edit);
    app.get('/api/comments/:id', CommentController.getOne);
    app.delete('/api/comments/:id', CommentController.delete);



    // to the route ../api/users
    app.get('/api/users', UserController.getAll);
    app.post('/api/users', UserController.create);
    app.get('/api/user/:id', UserController.getUserById);
    app.get('/api/user/:id/comments', CommentController.getAllCommentsByUserId);
    // app.put('/api/users/:id', UserController.edit);
    app.post('/api/login', UserController.login);
    app.get('/api/userid/:token', UserController.getUserIdFromToken);
    // app.delete('/api/users/:id', UserController.delete);
    app.put('/api/user/follow', UserController.follow);
    app.put('/api/user/unfollow', UserController.unfollow);
    app.get('/api/user/followlist/:id', UserController.followList);
};

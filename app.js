const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const routes = require('./src/routes/routes');
const cors = require('cors');
const connect = require('./connect')
require('dotenv').config();

const app = express();

mongoose.Promise = global.Promise;
if (process.env.NODE_ENV !== 'test') {
    console.log('Productie servers');
    connect.mongo(process.env.MONGO_URL)
    connect.neo(process.env.NEO4J_PROD_DB)
}

app.use(bodyParser.json());

app.use(cors());

routes(app);


app.use((err, req, res, next) => {
    // res.status(422).send({error: err.message});
});


module.exports = app;


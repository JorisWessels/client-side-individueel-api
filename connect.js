const neo_driver = require('./neo')
const mongoose = require('mongoose')

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}

async function mongo(dbName) {
    try {
        await mongoose.connect(`${process.env.MONGO_URL}/${dbName}`, options)
        console.log(`Connection to mongo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}

function neo(dbName) {
    try {
        neo_driver.connect(dbName)
        console.log(`Connection to neo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}

module.exports = {
    mongo,
    neo,
}

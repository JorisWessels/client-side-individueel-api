const neo4j = require('neo4j-driver')

function connect(dbName) {
    this.dbName = dbName
    this.driver = neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    )
}

function session() {
    return this.driver.session({
        database: this.dbName
    })
}

module.exports = {
    connect,
    session,
    dropAll: 'MATCH (n) DETACH DELETE n',
    getAllUsers: 'MATCH (n:User) RETURN COLLECT(n) ',
    createUser: 'CREATE (a:User { name: $name, password: $password, email: $email}) RETURN a',
    getUser: 'MATCH (n) WHERE n.email = $email RETURN n',
    getUserById: 'MATCH (n) WHERE ID(n) = $id RETURN n',
    followUser: 'MATCH (a:User), (b:User) WHERE a.name = $name AND b.name = $followName MERGE (a)-[:FOLLOWS]->(b) RETURN a, b',
    unfollowUser: 'MATCH (a:User)-[rel:FOLLOWS]->(b:User) WHERE a.name = $name AND b.name = $unfollowName DELETE rel RETURN a, b',
    followList: 'MATCH (n)-[rel:FOLLOWS]->(b:User) WHERE ID(n) = $id RETURN COLLECT(b)',
}

const mongoose = require('mongoose');
const connect = require('../connect');

// const neo = require('../neo');
// const Champion = require('../src/models/champion');
// const Item = require('../src/models/item');

before(done => {
    // mongoose.connect('mongodb://localhost/lolstats_test', {useNewUrlParser: true, useUnifiedTopology: true});
    mongoose.connect('mongodb+srv://Admin:frames98@client-side-individueel.tzefx.mongodb.net/lolstats_test?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    mongoose.connection
        .once('open', () => done())
        .on('error', error => {
            console.warn('Warning', error);
        });
    connect.neo(process.env.NEO4J_TEST_DB)

});

beforeEach((done) => {
    const {champions, items} = mongoose.connection.collections;
    items.drop(() => {
        champions.drop()
            .then(() => done())
            .catch(() => done());
    });

    // await Promise.all([Champion.deleteMany(), Item.deleteMany()])
    //
    // const session = neo.session()
    // await session.run(neo.dropAll)
    // await session.close();
});

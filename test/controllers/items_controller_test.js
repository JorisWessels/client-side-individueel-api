const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const Item = mongoose.model('item');

describe('Items controller', () => {
    it('Post to /api/items creates a new item', (done) => {
        Item.countDocuments()
            .then(count => {
                request(app)
                    .post('/api/items')
                    .send({
                        name: "Vuurbal",
                        description: "Hele grote vuurbal",
                        cost: 2300,
                        icon: "linkNaarIcon",
                        stats: {
                            attackdamage: 51,
                        }
                    })
                    .end(() => {
                        Item.countDocuments()
                            .then(newCount => {
                                assert(count + 1 === newCount);
                                done();
                            });
                    });
            });
    });
    it('Put to /api/items/id edits an existing item', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            }
        });

        item.save()
            .then(() => {
                request(app)
                    .put('/api/items/' + item._id)
                    .send({
                        description: "Andere text",
                    })
                    .end(() => {
                        Item.findOne({name: 'Vuurbal'})
                            .then(item => {
                                assert(item.description === "Andere text");
                                done();
                            });
                    });
            });
    });
    it('Delete to /api/items/id can delete an item', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            }
        });

        item.save()
            .then(() => {
                request(app)
                    .delete('/api/items/' + item._id)
                    .end(() => {
                        Item.findOne({name: "Vuurbal"})
                            .then((item) => {
                                assert(item === null);
                                done();
                            });
                    });
            });
    });
    it('Get to /api/items gets all items', (done) => {
        const item1 = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            }
        });
        const item2 = new Item({
            name: "Ijsbal",
            description: "Hele grote ijsbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 122,
            }
        });
        item1.save()
            .then(() => {
                request(app)
                    .get('/api/items')
                    .end(() => {
                        Item.countDocuments()
                            .then(count => {
                                assert(count === 1);
                            });
                    });
            });
        item2.save()
            .then(() => {
                request(app)
                    .get('/api/items')
                    .end(() => {
                        Item.countDocuments()
                            .then(count => {
                                assert(count === 2);
                                done();
                            });
                    });
            });
    });
    it('Get to /api/items/id gets one item by ID', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            }
        });

        item.save()
            .then(() => {
                request(app)
                    .get('/api/items/' + item._id)
                    .end(() => {
                        Item.findById({_id: item._id})
                            .then((item) => {
                                assert(item.name === "Vuurbal");
                                done();
                            });
                    });
            });
    });

    it('Saves comment as a sub document in an item', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            },
            comments: [
                {
                    dateofcomment: Date.now(),
                    content: "Coole champion!",
                    rating: 0
                }
            ]
        });

        item.save()
            .then(() => {
                request(app)
                    .get('/api/items/' + item._id)
                    .end(() => {
                        Item.findById({_id: item._id})
                            .then((item) => {
                                assert(item.comments[0].content === "Coole champion!");
                                done();
                            });
                    });
            });
    });
    it('Add a comment to an existing item', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            }
        });

        item.save()
            .then(() => Item.findOne({
                name: 'Vuurbal'
            }))
            .then((item) => {
                item.comments.push({
                        dateofcomment: Date.now(),
                        content: 'Coole champion!',
                        rating: 0
                    },
                    {
                        dateofcomment: Date.now(),
                        content: 'Stomme champion!',
                        rating: 0
                    })
                return item.save();
            })
            .then(() => Item.findOne({
                name: 'Vuurbal'
            }))
            .then((item) => {
                assert(item.comments[0].content === 'Coole champion!');
                done();
            });
    });
    it('Can remove an comment from an item', (done) => {
        const item = new Item({
            name: "Vuurbal",
            description: "Hele grote vuurbal",
            cost: 2300,
            icon: "linkNaarIcon",
            stats: {
                attackdamage: 51,
            },
            comments: [
                {
                    dateofcomment: Date.now(),
                    content: "Coole champion!",
                    rating: 0
                }
            ]
        });

        item.save()
            .then(() => Item.findOne({
                name: "Vuurbal"
            }))
            .then((item) => {
                item.comments[0].remove();
                return item.save();
            })
            .then(() => Item.findOne({
                name: "Vuurbal"
            }))
            .then((item) => {
                assert(item.comments.length === 0);
                done();
            });
    });
});

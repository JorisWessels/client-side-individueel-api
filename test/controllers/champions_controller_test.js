const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

const Champion = mongoose.model('champion');

describe('Champions controller', () => {
    it('Post to /api/champions creates a new champion', (done) => {
        Champion.countDocuments()
            .then(count => {
                request(app)
                    .post('/api/champions')
                    .send({
                        key: "5151",
                        name: "Runias",
                        title: "Fast Ninja",
                        tags: ["Fighter"],
                        stats: {
                            hp: 590,
                            hpperlevel: 90,
                            mp: 0,
                            mpperlevel: 0,
                            movespeed: 340,
                            armor: 38,
                            armorperlevel: 3.25,
                            spellblock: 20.1,
                            spellblockperlevel: 5.25,
                            attackrange: 130,
                            hpregen: 3,
                            hpregenperlevel: 1,
                            mpregen: 0,
                            mpregenperlevel: 0,
                            crit: 0,
                            critperlevel: 0,
                            attackdamage: 67,
                            attackdamageperlevel: 2,
                            attackspeedperlevel: 2.5,
                            attackspeed: 0.651
                        },
                        icon: "plaatje",
                        sprite: {
                            url: "plaatje",
                            x: 0,
                            y: 0,
                        },
                        description: "Snelle ninja"
                    })
                    .end(() => {
                        Champion.countDocuments()
                            .then(newCount => {
                                assert(count + 1 === newCount);
                                done();
                            });
                    });
            });
    });
    it('Put to /api/champions/id edits an existing champion', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });

        champion.save()
            .then(() => {
                request(app)
                    .put('/api/champions/' + champion._id)
                    .send({
                        name: "James",
                    })
                    .end(() => {
                        Champion.findOne({key: '5151'})
                            .then(champion => {
                                assert(champion.name === "James");
                                done();
                            });
                    });
            });
    });
    it('Delete to /api/champions/id can delete a champion', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });

        champion.save()
            .then(() => {
                request(app)
                    .delete('/api/champions/' + champion._id)
                    .end(() => {
                        Champion.findOne({key: "5151"})
                            .then((champion) => {
                                assert(champion === null);
                                done();
                            });
                    });
            });
    });
    it('Get to /api/champions gets all champions', (done) => {
        const champion1 = new Champion({
            key: "5151",
            name: "Runias1",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });
        const champion2 = new Champion({
            key: "5151",
            name: "Runias2",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });
        champion1.save()
            .then(() => {
                request(app)
                    .get('/api/champions')
                    .end(() => {
                        Champion.countDocuments()
                            .then(count => {
                                assert(count === 1);
                            });
                    });
            });
        champion2.save()
            .then(() => {
                request(app)
                    .get('/api/champions')
                    .end(() => {
                        Champion.countDocuments()
                            .then(count => {
                                assert(count === 2);
                                done();
                            });
                    });
            });
    });
    it('Get to /api/champions/id gets one champion by ID', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });

        champion.save()
            .then(() => {
                request(app)
                    .get('/api/champions/' + champion._id)
                    .end(() => {
                        Champion.findById({_id: champion._id})
                            .then((champion) => {
                                assert(champion.key === "5151");
                                done();
                            });
                    });
            });
    });
    it('Saves comment as a sub document in a champion', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja",
            comments: [
                {
                    dateofcomment: Date.now(),
                    content: "Coole champion!",
                    rating: 0
                }
            ]
        });

        champion.save()
            .then(() => {
                request(app)
                    .get('/api/champions/' + champion._id)
                    .end(() => {
                        Champion.findById({_id: champion._id})
                            .then((champion) => {
                                assert(champion.comments[0].content === "Coole champion!");
                                done();
                            });
                    });
            });
    });
    it('Add a comment to an existing champion', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja"
        });

        champion.save()
            .then(() => Champion.findOne({
                name: 'Runias'
            }))
            .then((champion) => {
                champion.comments.push({
                        dateofcomment: Date.now(),
                        content: 'Coole champion!',
                        rating: 0
                    },
                    {
                        dateofcomment: Date.now(),
                        content: 'Stomme champion!',
                        rating: 0
                    })
                return champion.save();
            })
            .then(() => Champion.findOne({
                name: 'Runias'
            }))
            .then((champion) => {
                assert(champion.comments[0].content === 'Coole champion!');
                done();
            });
    });
    it('Can remove an comment from a champion', (done) => {
        const champion = new Champion({
            key: "5151",
            name: "Runias",
            title: "Fast Ninja",
            tags: ["Fighter"],
            stats: {
                hp: 590,
                hpperlevel: 90,
                mp: 0,
                mpperlevel: 0,
                movespeed: 340,
                armor: 38,
                armorperlevel: 3.25,
                spellblock: 20.1,
                spellblockperlevel: 5.25,
                attackrange: 130,
                hpregen: 3,
                hpregenperlevel: 1,
                mpregen: 0,
                mpregenperlevel: 0,
                crit: 0,
                critperlevel: 0,
                attackdamage: 67,
                attackdamageperlevel: 2,
                attackspeedperlevel: 2.5,
                attackspeed: 0.651
            },
            icon: "plaatje",
            sprite: {
                url: "plaatje",
                x: 0,
                y: 0,
            },
            description: "Snelle ninja",
            comments: [
                {
                    dateofcomment: Date.now(),
                    content: "Coole champion!",
                    rating: 0
                }
            ]
        });

        champion.save()
            .then(() => Champion.findOne({
                name: "Runias"
            }))
            .then((champion) => {
                champion.comments[0].remove();
                return champion.save();
            })
            .then(() => Champion.findOne({
                name: "Runias"
            }))
            .then((champion) => {
                assert(champion.comments.length === 0);
                done();
            });
    });
});

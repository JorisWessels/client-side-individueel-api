const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');
const users_controller = require("../../src/controllers/users_controller");

const User = mongoose.model('user');

describe('Users controller', () => {

    it('Post to /api/users creates a new user', (done) => {
        request(app)
            .post('/api/users')
            .send({
                name: 'Klaas',
                password: 'secret',
                email: 'Klaas@gmail.com'
            })
            .end(() => {
                User.countDocuments()
                    .then(count => {
                        console.log(count);
                        assert(count);
                        done();
                    });
            });
        done();

        // User.countDocuments()
        //     .then(count => {
        //         console.log("count:", count);
        //         request(app)
        //             .post('/api/users')
        //             .send({
        //                 name: 'Klaas',
        //                 password: 'secret',
        //                 email: 'Klaas@gmail.com'
        //             })
        //             .end(() => {
        //                 User.countDocuments()
        //                     .then(newCount => {
        //                         console.log("newCount:", newCount);
        //                         assert(count + 1 === newCount);
        //                         done();
        //                     });
        //             });
        //         done();
        //     });

        // users_controller.create({
        //     name: 'Klaas',
        //     password: 'secret',
        //     email: 'Klaas@gmail.com'
        // })
        //     .then((user) => console.log("User:", user));
    });

    it('Get to /api/users gets all users', (done) => {
        request(app)
            .get('/api/users')
            .end(() => {
                User.countDocuments()
                    .then(count => {
                        assert(count === 0)
                        done();
                    });
            });
        done();
    });
});

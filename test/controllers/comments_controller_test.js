const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../../app');

// const Comment = mongoose.model('comment');

describe('Comments controller', () => {
    // it('Post to /api/comments creates a new Comment', (done) => {
    //     Comment.countDocuments()
    //         .then(count => {
    //             request(app)
    //                 .post('/api/comments')
    //                 .send({
    //                     dateofcomment: Date.now(),
    //                     content: "Leuke champion!",
    //                     rating: 0,
    //                 })
    //                 .end(() => {
    //                     Comment.countDocuments()
    //                         .then(newCount => {
    //                             assert(count + 1 === newCount);
    //                             done();
    //                         });
    //                 });
    //         });
    // });
    // it('Put to /api/comments/id edits an existing comment', (done) => {
    //     const comment = new Comment({
    //         dateofcomment: Date.now(),
    //         content: "Leuke champion!",
    //         rating: 0,
    //     });
    //
    //     comment.save()
    //         .then(() => {
    //             request(app)
    //                 .put('/api/comments/' + comment._id)
    //                 .send({
    //                     rating: 1,
    //                 })
    //                 .end(() => {
    //                     Comment.findOne({content: 'Leuke champion!'})
    //                         .then(comment => {
    //                             assert(comment.rating === 1);
    //                             done();
    //                         });
    //                 });
    //         });
    // });
    // it('Delete to /api/comments/id can delete a comment', (done) => {
    //     const comment = new Comment({
    //         dateofcomment: Date.now(),
    //         content: "Leuke champion!",
    //         rating: 0,
    //     });
    //
    //     comment.save()
    //         .then(() => {
    //             request(app)
    //                 .delete('/api/comments/' + comment._id)
    //                 .end(() => {
    //                     Comment.findOne({content: "Leuke champion!"})
    //                         .then((comment) => {
    //                             assert(comment === null);
    //                             done();
    //                         });
    //                 });
    //         });
    // });
    // it('Get to /api/comments gets all comments', (done) => {
    //     const comment1 = new Comment({
    //         dateofcomment: Date.now(),
    //         content: "Leuke champion!",
    //         rating: 0,
    //     });
    //     const comment2 = new Comment({
    //         dateofcomment: Date.now(),
    //         content: "Stomme champion!",
    //         rating: 0,
    //     });
    //     comment1.save()
    //         .then(() => {
    //             request(app)
    //                 .get('/api/comments')
    //                 .end(() => {
    //                     Comment.countDocuments()
    //                         .then(count => {
    //                             assert(count === 1);
    //                         });
    //                 });
    //         });
    //     comment2.save()
    //         .then(() => {
    //             request(app)
    //                 .get('/api/comments')
    //                 .end(() => {
    //                     Comment.countDocuments()
    //                         .then(count => {
    //                             assert(count === 2);
    //                             done();
    //                         });
    //                 });
    //         });
    // });
    // it('Get to /api/comments/id gets one comment by ID', (done) => {
    //     const comment = new Comment({
    //         dateofcomment: Date.now(),
    //         content: "Leuke champion!",
    //         rating: 0,
    //     });
    //
    //     comment.save()
    //         .then(() => {
    //             request(app)
    //                 .get('/api/comments/' + comment._id)
    //                 .end(() => {
    //                     Comment.findById({_id: comment._id})
    //                         .then((comment) => {
    //                             assert(comment.content === "Leuke champion!");
    //                             done();
    //                         });
    //                 });
    //         });
    // });
});
